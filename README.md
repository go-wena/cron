# cron

To download the specific tagged release, run:
```bash
go get gitee.com/go-wena/cron
```
Import it in your program as:
```go
import "gitee.com/go-wena/cron"
```
Refer to the documentation here: 
http://godoc.org/gitee.com/go-wena/cron

### Cron spec format

There are two cron spec formats in common usage:

- The "standard" cron format, described on [the Cron wikipedia page] and used by
  the cron Linux system utility.

- The cron format used by [the Quartz Scheduler], commonly used for scheduled
  jobs in Java software

[the Cron wikipedia page]: https://en.wikipedia.org/wiki/Cron
[the Quartz Scheduler]: http://www.quartz-scheduler.org/documentation/quartz-2.3.0/tutorials/tutorial-lesson-06.html

The original version of this package included an optional "seconds" field, which
made it incompatible with both of these formats. Now, the "standard" format is
the default format accepted, and the Quartz format is opt-in.

---
this lib is part of https://github.com/robfig/cron